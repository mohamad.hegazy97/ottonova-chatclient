export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const DELETE_MESSAGE = "DELETE_MESSAGE";
export const CLEAR_MESSAGES = "CLEAR_MESSAGE";

// socket actions
export const MESSAGE = "message";
export const COMMAND = "command";
// command types
export const COMPLETE = "complete";
export const RATE = "rate";
export const MAP = "map";
export const DATE = "date";
