import { ADD_MESSAGE, DELETE_MESSAGE, CLEAR_MESSAGES } from "../actions/actionTypes"
import {isEmpty} from '../../util/validation';

const initState = {
    messages: []
}

const messagesReducer = (state = initState, action) => {
    console.log(state.messages.length);
    if (action.type === ADD_MESSAGE) {
        if (isEmpty(action.value)) {
            return state;
        }
        const value = {
            ...action.value,
            id: state.messages.length + 1
        };

        let messages = [...state.messages, value];

        if (messages.length > 10) {
            messages = messages.slice(messages.length-10, messages.length);
        }
        
        return {
            ...state,
            messages: messages
        }
    } else if (action.type === DELETE_MESSAGE) {
        if (isEmpty(action.value)) {
            return state;
        }    
        return {
            ...state, 
            messages: state.messages.filter(message => message.id !== action.value)
        }
    } else if (action.type === CLEAR_MESSAGES) {
        return {
            ...state,
            messages: []
        }
    }

    return state;
}

 export default messagesReducer;