import { SET_CURRENT_USER } from "../actions/actionTypes"
import { isEmpty } from "../../util/validation"

const initState = {
    user: {},
    isAuth: false
}

const authReducer = (state = initState, action) => {
    if (action.type === SET_CURRENT_USER) {
        return {
            ...state,
            isAuth: !isEmpty(action.value),
            user: action.value
        }
    }

    return state;
}

 export default authReducer;