


export const checkUserName = (userName) => {
    
    if (isEmpty(userName) || userName.length < 6) {
        return false;
    }
    return true;

};
    
export const checkPassword = (password) => {
    if (isEmpty(password)) {
        return false;
    }
    // this can be change regarding the password policy !
    if (password.match("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+*!=]).*$")) {
        return true;
    }

    return false;
}

export const isEmpty = (value) => {
    return (value === undefined || 
        value === null || 
        (typeof value === "object" && Object.keys(value).length === 0) ||
        (typeof value === "string" && value.trim().length === 0))
}
