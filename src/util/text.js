const HelperText = {
    ALERT_TEXT : "You have entered invalid data.\n Please check the errors.",
    ERROR_TEXT : "Something is wrong, please check your credentials.",
    LOGIN_TEXT : "Login",
    CHAT_INPUT_HOLDER : "you can talk or add a command here to check the command please add 'command' word to the text"
}

export default HelperText;