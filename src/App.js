import React from 'react'
import ChatBot from './containers/ChatBot';
import Login from './containers/Login';
import PrivateRoute from './common/PrivateRoute';
import './assets/css/App.css';
import {Route, Switch, withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import NotFound from './containers/NotFound';

const App = () => {
  return (
    <div className={'App'}>
      <Switch>
        <Route exact path={'/'} component={Login} />
        <PrivateRoute exact path={'/bot'} component={ChatBot} />
        <Route component={NotFound} />
      </Switch>
    </div>
  )
}

const mapStateToProps = state=> {
  return {
    auth: state.auth
  }
}

export default withRouter(connect(mapStateToProps)(App));