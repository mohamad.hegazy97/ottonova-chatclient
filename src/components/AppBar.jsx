import * as React from 'react';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import LogoutIcon  from '@mui/icons-material/Logout';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import MenuItem from '@mui/material/MenuItem';
import Toolbar from '@mui/material/Toolbar';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import Box from '@mui/material/Box';
import { CLEAR_MESSAGES } from '../store/actions/actionTypes';
import { connect, useDispatch } from 'react-redux';
import { withRouter } from 'react-router';
import '../assets/css/indecator.css';
import socket from '../socket-io';

const NavBar = (props) => {

  const dispatch = useDispatch();

  const user = props.auth.user;

  const [anchorEl, setAnchorEl] = React.useState(null);
  
  const isMenuOpen = Boolean(anchorEl);

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
      socket.disconnect() 
      dispatch({type: CLEAR_MESSAGES, value: {}});
      props.history.push('/');
  }

  const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={isMenuOpen}
        onClose={handleMenuClose}
      >
        <MenuItem onClick={handleLogout}>
          <ListItemIcon>
              <LogoutIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText>Logout</ListItemText>
        </MenuItem>
      </Menu>
  );
  
  return (
    <Box sx={{ flexGrow: 1 }} className="indecator">
      <AppBar position="fixed">
        <Toolbar>
          <Typography
            variant="h6"
            noWrap
            component="div"
          >
            Ottonova chatbot
          </Typography>
          <Box sx={{ flexGrow: 1 }} />
          <Box>
            <IconButton
              size="large"
              edge="end"
              aria-label="user account"
              aria-haspopup="true"
              onClick={handleMenuOpen}
              id="user_profile_menu"
              color="inherit"
            >
              <Avatar alt={user.author} />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMenu}
    </Box>
  );
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
    };
};
 
export default withRouter(connect(mapStateToProps)(NavBar));