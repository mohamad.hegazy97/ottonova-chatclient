import * as React from 'react';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import SendIcon from '@mui/icons-material/Send';
import { ADD_MESSAGE, COMMAND, MESSAGE } from '../store/actions/actionTypes';
import { isEmpty } from '../util/validation';
import { useDispatch } from 'react-redux';
import HelperText from '../util/text';
import { connect } from 'react-redux';
import socket from '../socket-io';

const CommandBar = (props) => {
  const dispatch = useDispatch();

  const onSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    if (isEmpty(data.get('message'))) {
      return;
    }
    if (data.get('message').search("command") === 0) {
      socket.emit(COMMAND, {author: props.auth.user.author});
    } else {
      socket.emit(MESSAGE, {author: props.auth.user.author, message: data.get("message")});
      dispatch({type: ADD_MESSAGE, value: {msg: data.get('message'), isMainUser: true, type: MESSAGE}});
    }
    event.currentTarget.reset();

  }

  return (
    <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={18}>
      <Paper
        component="form"
        onSubmit={onSubmit}
        sx={{ p: '2px 4px', display: 'flex', alignItems: 'center' }}
      >
        <InputBase
          name="message"
          id="message"
          fullWidth 
          sx={{ flex: 1 }}
          placeholder={HelperText.CHAT_INPUT_HOLDER}
        />
        <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
        <IconButton type="submit" color="primary" sx={{ p: '10px' }} aria-label="send">
          <SendIcon />
        </IconButton>
      </Paper>
    </Paper>
  );
}

const mapStateToProps = state => {
  return {
      auth: state.auth,
  };
};

export default connect(mapStateToProps)(CommandBar);