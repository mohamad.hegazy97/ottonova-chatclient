   
import React, { useRef, useEffect } from 'react'
import { ADD_MESSAGE, COMMAND, MESSAGE } from '../store/actions/actionTypes';
import { connect, useDispatch } from 'react-redux';
import "../assets/css/messageBox.css";
import socket from '../socket-io';
import Message from './Message';

const MessagesBox = (props) => {

    const dispatch = useDispatch();

    const messages = props.msg.messages;

    useEffect(()=>{
      socket.on(MESSAGE, ({author, message}) => {
          dispatch({type: ADD_MESSAGE, value: {msg: message, isMainUser: false, type: MESSAGE, payload: ""}});
      });
      socket.on(COMMAND, ({autor, command}) => {
          dispatch({type: ADD_MESSAGE, value: {msg: "", isMainUser: false, type: command.type, payload: command.data}});
      })
    }, []);
  
    const endDiv = useRef(null)

    useEffect(() => {
        endDiv.current.scrollIntoView()
    }, [messages])

    return (
        <div className="chats">
            {messages
                .map((m) => (
                    <Message message={m} key={m.id} />
                ))}
            <div style={{ float: 'right', clear: 'both' }} ref={endDiv}></div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        msg: state.message,
    };
};

export default connect(mapStateToProps)(MessagesBox);