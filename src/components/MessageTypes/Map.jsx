import React from 'react'
import GoogleMapReact from 'google-map-react';
import Box from '@mui/material/Box';

const Map = (props) => {
    const center= props.center;
    const zoom = 11;
    return (
        <Box style={{width:"500px", height: "200px"}}>
            <GoogleMapReact
                defaultCenter={center}
                defaultZoom={zoom}
            >
                <div
                    lat={center.lat}
                    lng={center.lng}
                    text="Marker"
                ></div>
            </GoogleMapReact>
        </Box>
    )
}

export default Map;
