import * as React from 'react';
import Typography from '@mui/material/Typography';
import Rating from '@mui/material/Rating';
import Box from '@mui/material/Box';
import { MESSAGE, DELETE_MESSAGE } from '../../store/actions/actionTypes';
import { connect, useDispatch } from 'react-redux';
import socket from '../../socket-io';

const Rate = (props) => {
  const dispatch = useDispatch();

  const maxRate = props.range.max - props.range.min;

  const rate = (event, val) => {
    socket.emit(MESSAGE, {author: props.auth.user.author, message: val});
    dispatch({type: DELETE_MESSAGE, value: props.id});
  };

  return (
    <Box>
      <Typography component="legend">Please rate your experience</Typography>
      <Rating name="Rate" max={maxRate} onChange={rate}/>
    </Box>
  );
}


const mapStateToProps = state => {
  return {
      auth: state.auth,
  };
};

export default connect(mapStateToProps)(Rate);