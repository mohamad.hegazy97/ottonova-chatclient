import React from 'react'
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import { MESSAGE, CLEAR_MESSAGES, ADD_MESSAGE } from '../../store/actions/actionTypes';
import { useDispatch } from 'react-redux';
import { connect } from 'react-redux';
import socket from '../../socket-io';

const DateMessage = (props) => {
    const dispatch = useDispatch();

    const handleClick = (event) => {
        socket.emit(MESSAGE, {author: props.auth.user.author, message: event.currentTarget.innerText.toLowerCase()})
        dispatch({type: CLEAR_MESSAGES});
        dispatch({type: ADD_MESSAGE, value: {msg: props.auth.user.author + " " + event.currentTarget.innerText.toLowerCase(), isMainUser: false, type: MESSAGE}});
    }

    let date = new Date(props.date);
    let days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
    days = [...days, ...days];
    const startingIndex = (date.getDay() % 6 + 1) - 2 > 0 ? 
                            (date.getDay() % 6 + 1) - 2 : (date.getDay() % 6 + 1) - 1;
    days = days.slice(startingIndex, startingIndex + 5);

    return (
        <ButtonGroup variant="contained" aria-label="outlined primary button group">
            {days.map((day)=> {
                return (
                    <Button onClick={handleClick}>
                        {day}
                    </Button>
                )
            })}
        </ButtonGroup>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
    };
};

export default connect(mapStateToProps)(DateMessage);
