import React from 'react'
import ButtonGroup from '@mui/material/ButtonGroup';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { MESSAGE, DELETE_MESSAGE, CLEAR_MESSAGES } from '../../store/actions/actionTypes';
import { connect, useDispatch } from 'react-redux';
import { withRouter } from 'react-router';
import socket from '../../socket-io';

const Complete = (props) => {

    const dispatch = useDispatch();

    const handleClick = (event) => {
        socket.emit(MESSAGE, {author: props.auth.user.author, message: event.currentTarget.innerText.toLowerCase()})
        if (event.currentTarget.innerText === "YES") {
            socket.disconnect() 
            dispatch({type: CLEAR_MESSAGES, value: {}});
            props.history.push('/');
        }
        dispatch({type: DELETE_MESSAGE, value: props.id});
    }

    const options = props.options;
    
    return (
        <Box>
            <Typography component="legend">Did u finish with the widget ?</Typography>
            <ButtonGroup variant="contained" aria-label="outlined primary button group">
                {options.map((option)=> {
                    return (
                        <Button onClick={handleClick}>
                            {option}
                        </Button>
                    )
                })}
            </ButtonGroup>
        </Box>
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
    };
};

export default withRouter(connect(mapStateToProps)(Complete));