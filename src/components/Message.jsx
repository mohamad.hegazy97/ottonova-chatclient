import React from 'react'
import Typography from '@mui/material/Typography';
import Box from "@mui/material/Box";
import { MESSAGE, MAP, RATE, DATE, COMPLETE } from '../store/actions/actionTypes';
import DateMessage from './MessageTypes/DateMessage';
import Complete from './MessageTypes/Complete';
import Rate from './MessageTypes/Rate';
import Map from './MessageTypes/Map';
import "../assets/css/message.css";

export default function Message({ message }) {
    
    let showMessage = null;
    if (message.type === MESSAGE) {
        showMessage = (
            message.msg
        );
    } else if (message.type === MAP) {
        showMessage = (
            <Box>
                <Typography component="legend">You can reach here for more support</Typography>
                <Map center={message.payload} id={message.id}/>
            </Box>
        );
    } else if (message.type === RATE) {
        showMessage = (
            <Rate range={{min: message.payload[0], max: message.payload[1]}} id={message.id} />
        );
    } else if (message.type === DATE) { 
        showMessage = (
            <DateMessage date={message.payload} id={message.id} />
        );
    } else if (message.type === COMPLETE) {
        showMessage = (
            <Complete options={message.payload} id={message.id}/>
        );
    }

    return (
        <div className={`message ${message.isMainUser ? 'sent' : 'received'}`}>
            {showMessage}
        </div>
    )
}