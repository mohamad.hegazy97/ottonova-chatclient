import * as React from 'react';
import { useDispatch } from 'react-redux';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import AlertTitle from '@mui/material/AlertTitle';
import Toolbar from '@mui/material/Toolbar';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import {checkUserName, checkPassword} from "../util/validation";
import { SET_CURRENT_USER } from '../store/actions/actionTypes';
import InputField from "../common/InputField";
import HelperText from '../util/text';
import "../assets/css/indecator.css";
import "../assets/css/login.css";

const Login = (props) => {

  const dispatch = useDispatch()
  const [validation, setValidation] = React.useState(false);
  const [helperText, setHelperText] = React.useState("");
  const [alertText, setAlertText] = React.useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const inputValidation = checkUserName(data.get('username')) && checkPassword(data.get('password'));
    
    setValidation(!inputValidation);
    if (!inputValidation) {
      setAlertText(HelperText.ALERT_TEXT);
      setHelperText(HelperText.ERROR_TEXT);
      return;
    }
    
    setHelperText("");
    setAlertText("");
    
    dispatch({type: SET_CURRENT_USER, value: {author: data.get('username')}});
    props.history.push('/bot');
  };

  return (
    <Grid container component="main" sx={{ height: '100vh' }} className="indecator">
    <CssBaseline />
    <Grid
      item
      xs={false}
      sm={4}
      md={9}
      className="mainGrid"
    />
    <Grid item xs={12} sm={8} md={3} component={Paper} elevation={6} square>
      <Box sx={{
          my: 8,
          mx: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}>
        <Toolbar/>
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {HelperText.LOGIN_TEXT}
        </Typography>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
         
          <InputField
            name="username"
            label="username"
            type="text"
            id="username"
            error={validation}
            helperText={helperText}
          />
          
          <InputField
            name="password"
            label="Password"
            type="password"
            id="password"
            error={validation}
            autoComplete="current-password"
            helperText={helperText}
          />
          {validation && <Alert severity="error" sx={{bgcolor:"#f8bbd0"}}>
                <AlertTitle>Error</AlertTitle>
                {alertText}
              </Alert>
          }
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            {HelperText.LOGIN_TEXT}
          </Button>
        </Box>
      </Box>
    </Grid>
  </Grid>
  );
}

export default Login;