import * as React from 'react';
import {Box, CssBaseline, Toolbar} from '@mui/material';
import CommandBar from "../components/CommandBar";
import MessageBox from "../components/MessageBox";
import AppBar from '../components/AppBar';

const ChatBot = () => {
  return (
    <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBar />
        <Box component="main" sx={{ flexGrow: 100, p: 3 }}>
            <Toolbar />
            <MessageBox/>
            <CommandBar />
        </Box>
    </Box>
  );
}

export default ChatBot;