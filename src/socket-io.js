import { io } from "socket.io-client";

const socket = io.connect('https://demo-chat-server.on.ag/');

export default socket;