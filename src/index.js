import React from 'react';
import App from './App';
import { createStore, combineReducers } from "redux";
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "react-redux";
import ReactDOM from 'react-dom';
import messagesReducer from './store/reducers/chatReducer';
import authReducer from './store/reducers/authReducer';
import './index.css';

const rootReducer = combineReducers({
  auth: authReducer,
  message: messagesReducer
});

const store = createStore(rootReducer);

const app = (
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
)

ReactDOM.render(app, document.getElementById('root'));