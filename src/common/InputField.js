import React from 'react'
import TextField from '@mui/material/TextField';

const InputField = (props) => {
    
    const {error, id, label, name, autoComplete, helperText, type} = props;

    return (
        <TextField
            margin="normal"
            required
            fullWidth
            autoFocus
            type={type}
            id={id}
            label={label}
            name={name}
            autoComplete={autoComplete}
            error={error}
            helperText={helperText}
        />
      )
}

export default InputField;